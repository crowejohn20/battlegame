﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Delay : MonoBehaviour
{
   public IEnumerator WaitForSeconds(float seconds)
    {
        // Not AI simple delay for demo purpose. MiniMax AI logic might work well here.
        
        yield return new WaitForSeconds(seconds);

        //EnemyAttack();
    }
}
