﻿using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class CharacterSelectUI : MonoBehaviour
{
    #region Fields
    public Image characterImageMain;
    public TextMeshProUGUI characterDescription;
    public TextMeshProUGUI characterName;
    public TextMeshProUGUI characterClass;
    public TextMeshProUGUI characterGroup;
    public Button selectCharacter;
    public Button next;
    public Button previous;
    public Button startButton;
    public Image[] miniImages;
    #endregion

    #region Methods
    public void PopulateCharacterInfo(Character characterInfo)
    {
        IClassDetails ClassInformation = characterInfo.GetComponent<IClassDetails>();

        characterImageMain.sprite = characterInfo.Image;
        characterName.text = characterInfo.CharacterName;
        characterDescription.text = ClassInformation.ClassDetails()["Ability"];

        characterGroup.text = ClassInformation.ClassDetails()["Group"];
        characterClass.text = ClassInformation.ClassDetails()["Class"];

    }

    #endregion
}
