using System.Collections.Generic;
using UnityEngine;

public enum SupportGroups { Genie, Angel }
public class Support : Character, IAbility, IClassDetails
{
    public string _characterText;
    public SupportGroups supportGroups;
    public string CharacterClass = "Support";

    private int round;
    private int buffedRound;
    private Dictionary<string, string> _classDetails;
    private Dictionary<string, int> _genieBuffs = new Dictionary<string, int>()
    {
        {"Heal", 5},
        {"Extra Damage", 5}
    };

    public void UpdateDetails()
    {
        switch (supportGroups)
        {
            case SupportGroups.Genie:
                _characterText = "Every third round the genie can grant a wish:" +
                                    " - Heal a unit for 5 life" +
                                    " - Give a unit +5 damage for 1 round" +
                                    " - Deal 3 damage to an enemy";
                break;
            case SupportGroups.Angel:
                _characterText = "Angels can’t attack. Every round they can bless another unit." +
                                 "A blessing will heal the unit for 3 life per round for 3 rounds." +
                                 "Angels can’t heal themselves";
                break;
        }
    }

    public Dictionary<string, string> ClassDetails()
    {
        UpdateDetails();

        _classDetails = new Dictionary<string, string>();

        _classDetails.Add("Class", CharacterClass);
        _classDetails.Add("Group", supportGroups.ToString());
        _classDetails.Add("Ability", _characterText);

        return _classDetails;
    }

    public void Ability(GameManager manager, Character enemy)
    {
        round = manager.currentRound;

        switch (supportGroups)
        {
            case SupportGroups.Genie:
                if (true)
                //if (round % 3 == 0)
                {
                    string buff = SelectBuffType();

                    if (buff != null)
                    {
                        AttemptBuff(manager, buff);
                        break;
                    }

                    else
                    {
                        Attack(enemy);
                        break;
                    }
                }

                else
                {
                    Attack(enemy);
                }
                break;
        }
    }

    private void AttemptBuff(GameManager manager, string buff)
    {
        Character target = SelectBuffCharacter(manager);
        UpdateUI.TextLogger("GENIE ABILITY: Granting a wish. Applying " + buff + " to " + target.CharacterName + " " + _genieBuffs["Heal"]);

        if (!target.Buffed)
            ApplyBuff(target, buff);
        else
            UpdateUI.TextLogger("Buff Failed " + target.CharacterName + " already buffed");

    }

    private Character SelectBuffCharacter(GameManager manager)
    {
        Character _charactersToBuff;
        GetRandomCharacter randomCharacterToBuff = manager.GetComponent<GetRandomCharacter>();

        if (manager._gameState == GameManager.GameState._playersTurn)
            _charactersToBuff = randomCharacterToBuff.RandomCharacter(manager.playerCharacters);
        else
            _charactersToBuff = randomCharacterToBuff.RandomCharacter(manager.CPUCharacters);

        return _charactersToBuff;

    }

    private string SelectBuffType()
    {
        bool _doDamageOrBuff = (Random.value > 0.5f);

        if (_doDamageOrBuff)
        {
            UpdateUI.TextLogger("Buff attempt failed- Dealing damage instead");
            return null;
        }

        Random rand = new Random();
        List<string> keys = new List<string>(_genieBuffs.Keys);
        return keys[Random.Range(0, keys.Count)];
    }

    private void ApplyBuff(Character toBuff, string buffKey)
    {
        buffedRound = round;

        if (buffKey == "Heal")
            toBuff.Heal(_genieBuffs["Heal"]);
        if (buffKey == "Extra Damage")
            toBuff.DamageBonus(_genieBuffs["Extra Damage"], round);

    }

    public void GroupAbility()
    {
        //Debug.Log("Support characters can’t be targeted if there is a fighter alive on the team.");
    }
}
