﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deck : MonoBehaviour
{
    public GameObject deck;
    private int charactersInDeck;

    public List<Character> CreateDeck()
    {
        charactersInDeck = deck.transform.childCount;
        List<Character> characterList = new List<Character>();

        if (charactersInDeck >= 8 && charactersInDeck % 2 == 0)
        {
            foreach (Transform character in deck.transform)
            {
                character.gameObject.name = character.GetComponent<Character>().CharacterName;
                characterList.Add(character.GetComponent<Character>());
            }

            return (characterList);
        }

        else
        {
            Debug.Log("Deck of size " + charactersInDeck + " is not big enough for a two player game");
            return null;
        }
    }
}
