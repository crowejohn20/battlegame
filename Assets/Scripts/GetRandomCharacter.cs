﻿using System.Collections.Generic;
using UnityEngine;

public class GetRandomCharacter : MonoBehaviour
{
    public Character RandomCharacter(List<Character> currentTeam)
    {
        int randomPlayerIndex = Random.Range(0, currentTeam.Count);
        Character _randomCharacter = currentTeam[randomPlayerIndex].GetComponent<Character>();

        return _randomCharacter;
    }
}
