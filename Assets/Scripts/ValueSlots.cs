﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ValueSlots : MonoBehaviour
{
    public TextMeshProUGUI nameSlot;
    public TextMeshProUGUI classSlot;
    public TextMeshProUGUI healthSlot;
    public TextMeshProUGUI DamageSlot;
    public Image ImageSlot;
    public GameObject character;
    public GameObject background;

}
