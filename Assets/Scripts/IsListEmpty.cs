﻿using System.Linq;
using System.Collections.Generic;

public static class IsListEmpty
{
    public static bool IsEmpty<T>(List<T> list)
    {
        if (list == null) { return true; }

        return !list.Any();
    }
}
