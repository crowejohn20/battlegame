using System.Collections.Generic;
public interface IClassDetails
{
    Dictionary<string, string> ClassDetails();

    void UpdateDetails();
}