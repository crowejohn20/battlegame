using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections;

public class UpdateUi : MonoBehaviour
{
    public TextMeshProUGUI roundNumber;
    public GameObject enemyTeam;
    public GameObject playerTeam;
    public GameObject TextBox;

    private TextMeshProUGUI _text;
    private List<string> _result;
    private ValueSlots _cpuValues;
    private ValueSlots _playerValues;
    [SerializeField]
    private GameManager _gameManager;


    private void Awake()
    {
        _result = new List<string>();
        _text = TextBox.GetComponent<TextMeshProUGUI>();
        _gameManager = GetComponent<GameManager>();
        
    }

    public void FixedUpdate()
    {
        UpdateCanvasText();
    }

    public void UpdateRound(string round)
    {
        roundNumber.text = round;
    }

    public void TextLogger(string text)
    {
        _result.Add(text += "\n");

        if (_result.Count > 4)
        {
            _text.text = "";
            _text.fontStyle = FontStyles.Bold;
            _result.RemoveRange(0, 2);

            foreach (string t in _result) { _text.text += t; }
        }

        else
        {
            _text.text += _result[_result.Count - 1];
        }
    }

    public void UpdateCanvasText()
    {
        for (int i = 0; i < _gameManager.CPUCharacters.Count; i++)
        {
            IClassDetails _buttonInformation = _gameManager.CPUCharacters[i].GetComponent<IClassDetails>();

            Character cpuCharacter = _gameManager.CPUCharacters[i];
            _cpuValues = enemyTeam.transform.GetChild(i).GetComponent<ValueSlots>();
            _cpuValues.character = cpuCharacter.gameObject;
            _cpuValues.nameSlot.text = cpuCharacter.CharacterName;
            _cpuValues.ImageSlot.sprite = cpuCharacter.Image;
            _cpuValues.DamageSlot.text = cpuCharacter.Damage.ToString();
            _cpuValues.healthSlot.text = cpuCharacter.Health.ToString();
            _cpuValues.classSlot.text = _buttonInformation.ClassDetails()["Group"];
        }

        for (int i = 0; i < _gameManager.playerCharacters.Count; i++)
        {
            IClassDetails _labelInformation = _gameManager.playerCharacters[i].GetComponent<IClassDetails>();
            Character _playerCharacter = _gameManager.playerCharacters[i];

            _playerValues = playerTeam.transform.GetChild(i).GetComponent<ValueSlots>();
            _playerValues.character = _playerCharacter.gameObject;
            _playerValues.nameSlot.text = _playerCharacter.CharacterName;
            _playerValues.ImageSlot.sprite = _playerCharacter.Image;
            _playerValues.DamageSlot.text = _playerCharacter.Damage.ToString();
            _playerValues.healthSlot.text = _playerCharacter.Health.ToString();
            _playerValues.classSlot.text = _labelInformation.ClassDetails()["Group"];
        }
    }

    public void HightlightCharacter(Character currentCharacter)
    {
        Color c = new Color(0, 0, 255, 60);
        Color original = new Color(0, 0, 0, 60);
        Image background;

        foreach (Transform t in currentCharacter.transform.parent)
        {
            background = t.GetComponentInParent<ValueSlots>().background.GetComponent<Image>();
            background.color = original;
        }

        background = currentCharacter.GetComponentInParent<ValueSlots>().background.GetComponent<Image>();
        background.color = c;


    }

    public void RemoveCharacter(Character obj, List<Character> currentDeck)
    {
        if (currentDeck.Contains(obj))
        {
            Destroy(obj.transform.parent.gameObject);
            currentDeck.Remove(obj);
        }
    }
}
