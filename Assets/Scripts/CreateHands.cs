﻿using System.Collections.Generic;
using UnityEngine;
public class CreateHands : MonoBehaviour
{
    private List<Character> newDeck;
    private UpdateUi updateUi;

    public List<Character> CreateHand(List<Character> deck, Transform currentTeam)
    {
        newDeck = new List<Character>();

        foreach (Transform transform in currentTeam)
        {
            GameObject _characterSlot = transform.GetComponent<ValueSlots>().character;
            int characterIndex = Random.Range(0, deck.Count);

            GameObject randomCharacter = Instantiate(deck[characterIndex].gameObject, Vector3.up, Quaternion.identity, transform.transform);

            _characterSlot = randomCharacter;
            transform.gameObject.name = randomCharacter.name;

            deck.RemoveAt(characterIndex);
            newDeck.Add(randomCharacter.GetComponent<Character>());
        }
        return newDeck;
    }

    public List<Character> InstantiatePlayerCharacters(List<Character> characters, Transform currentTeam)
    {
        newDeck = new List<Character>();

        for (int i = 0; i < characters.Count; i++)
        {
            GameObject _characterSlot = currentTeam.GetChild(i).GetComponent<ValueSlots>().character;

            GameObject characterInstance = Instantiate(characters[i].gameObject, Vector3.up, Quaternion.identity, currentTeam.GetChild(i));
            currentTeam.GetChild(i).transform.name = characterInstance.name;

            newDeck.Add(characterInstance.GetComponent<Character>());
        }

        return newDeck;
    }
}
