using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace CharacerSelectMenu
{
    public class CharacterSelect : MonoBehaviour
    {
        #region Fields
        [SerializeField]
        private GameManager _gameManager;
        [SerializeField]
        private GameObject gameCanvas;
        private CharacterSelectUI _characterUI;
        private Deck _deck;
        private int startingPosition;
        private List<Character> charactersSelected;
        private List<Character> _players;
        #endregion

        #region Methods
        void Awake()
        {
            _deck = GetComponent<Deck>();
            _characterUI = GetComponent<CharacterSelectUI>();

            _players = _deck.CreateDeck();
            charactersSelected = new List<Character>();
        }

        private void Start()
        {
            Character defaultCharacter = _players[0];
            _characterUI.PopulateCharacterInfo(defaultCharacter);
        }

        public void GoToCharacter(int direction)
        {
            startingPosition += direction;

            if (startingPosition >= 0 && startingPosition < _players.Count)
            {
                Character currentCharcter = _players[startingPosition];
                _characterUI.PopulateCharacterInfo(currentCharcter);
            }

            else
            {
                startingPosition -= direction;
            }
        }

        public void SelectCharacter()
        {
            for (int i = 0; i < _characterUI.miniImages.Length; i++)
            {
                Sprite currentSprite = _characterUI.miniImages[i].sprite;

                if (currentSprite == null)
                {
                    _characterUI.miniImages[i].sprite = _players[startingPosition].Image;
                    charactersSelected.Add(_players[startingPosition].GetComponent<Character>());
                    break;
                }
            }

            CheckStartEnabled();
        }

        public void DeselectCharacter()
        {
            GameObject deselect = EventSystem.current.currentSelectedGameObject.transform.parent.gameObject;
            Image removeImage = deselect.GetComponent<Image>();

            if (removeImage.sprite == null) { return; }
            {
                removeImage.sprite = null;

                for (int i = 0; i < deselect.transform.childCount; i++)
                {
                    if (deselect.transform.GetChild(i) == EventSystem.current.currentSelectedGameObject.transform)
                    {
                        charactersSelected.RemoveAt(i);
                    }
                }
            }

            CheckStartEnabled();
        }

        private void CheckStartEnabled()
        {
            if (charactersSelected.Count == 4)
            { _characterUI.startButton.gameObject.SetActive(true); return; }

            _characterUI.startButton.gameObject.SetActive(false);
        }

        public void StartGame()
        {
            gameObject.SetActive(false);
            gameCanvas.gameObject.SetActive(true);

            _gameManager.StartGame(charactersSelected);
        }
        #endregion
    }
}

