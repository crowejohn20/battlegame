using UnityEngine;

public class Character : MonoBehaviour
{
    #region Fields
    private UpdateUi _ui;

    [SerializeField]
    private Sprite _image;

    [SerializeField]
    private string _characterName;

    [SerializeField]
    private int _health;

    [SerializeField]
    private int _damage;
    private bool _isTargetable = true;
    private bool _dead;
    private bool _buffed;
    private int _roundBuffed;
    private int _damageBuffBonus;

    #endregion

    #region Properties
    public string CharacterName
    {
        get { return _characterName; }
    }

    public int Health
    {
        get { return _health; }
    }

    public bool Dead
    {
        get { return _dead; }
    }

    public bool Buffed
    {
        get { return _buffed; }
    }

    public int RoundBuffed
    {
        get { return _roundBuffed; }
    }

    public int Damage
    {
        get { return _damage; }
    }

    public Sprite Image
    {
        get { return _image; }
    }

    public UpdateUi UpdateUI
    {
        get { return _ui; }
    }

    #endregion

    #region Constructors
    public Character()
    {

    }

    #endregion

    #region Methods

    public void Awake()
    {
        _ui = GameObject.Find("GameManager").GetComponent<UpdateUi>();
    }

    public void Attack(Character enemy)
    {
        if (CheckEnemyState(enemy))
        {
            _ui.TextLogger(CharacterName + " is attacking doing " + Damage + " damage to " + enemy.CharacterName + "!" + "(" + enemy.Health + ")");

            enemy._health -= _damage;
            IsDead(enemy);
        }
    }

    private void IsDead(Character enemy)
    {
        if (enemy._health > 0)
            _ui.TextLogger(enemy.CharacterName + " is still alive with " + enemy._health + " health!");

        else
        {
            _ui.TextLogger(enemy.CharacterName + " has been killed ");
            enemy._dead = true;
            return;
        }
    }

    public void IsTargetable(Character enemy)
    {
        enemy._isTargetable = !enemy._isTargetable;
    }

    public void Heal(int value)
    {
        _health += value;
        //_updateUI.TextLogger(CharacterName + " is being healed " + value + "to health");
    }

    public void DamageBonus(int value, int round)
    {
        _damageBuffBonus = value;
        _damage += _damageBuffBonus;

        _buffed = true;
        _roundBuffed = round;

        //_updateUI.TextLogger(CharacterName + " had damage buff of " + value + " applied");
    }

    public void RemoveDamageBonus()
    {
        _damage -= _damageBuffBonus;
        _buffed = false;
    }

    private bool CheckEnemyState(Character state)
    {
        if (state._dead || !state._isTargetable)
            return false;
        return true;
    }

    #endregion

}