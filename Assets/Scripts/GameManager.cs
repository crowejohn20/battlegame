using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GameManager : MonoBehaviour
{
    #region Fields
    public GameObject teams;
    [SerializeField]
    public List<Character> playerCharacters;
    [SerializeField]
    public List<Character> CPUCharacters;
    public int currentRound { get; private set; }
    public enum GameState { _playersTurn, _cpuTurn, _gameOver };
    public GameState _gameState { get; private set; }

    private Deck _deck;
    private Character currentEnemy;
    private Character currentPlayer;
    private Character characterClicked;
    private CreateHands _createHands;
    private UpdateUi _updateUi;
    private GetRandomCharacter _getRandomCharacter;
    private GameManager _gameManager;
    
        
#endregion

#region Methods
private void Awake()
    {
        _gameManager = GetComponent<GameManager>();
        _createHands = gameObject.GetComponent<CreateHands>();
        _getRandomCharacter = gameObject.AddComponent<GetRandomCharacter>();

        _deck = gameObject.GetComponent<Deck>();
        _updateUi = gameObject.GetComponent<UpdateUi>();

    }

    // FOR TESTING - Game should call StartGame via menu.
    public void Start()
    {     
        playerCharacters = _createHands.CreateHand(_deck.CreateDeck(), teams.transform.GetChild(0));
        CPUCharacters = _createHands.CreateHand(_deck.CreateDeck(), teams.transform.GetChild(1));

        UpdateRound();

    }

    public void StartGame(List<Character> playersSelected)
    {
        playerCharacters = _createHands.InstantiatePlayerCharacters(playersSelected, teams.transform.GetChild(0));
        CPUCharacters = _createHands.CreateHand(_deck.CreateDeck(), teams.transform.GetChild(1));

        UpdateRound();
    }

    public void GetEnemyButtonClicked()
    {
        if (_gameState != GameState._playersTurn) { return; }

        GameObject buttonClicked = EventSystem.current.currentSelectedGameObject;
        characterClicked = buttonClicked.GetComponentInParent<ValueSlots>().character.GetComponent<Character>();

        IAbility _playerAbility = currentPlayer.GetComponent<IAbility>();

        StartAttack(characterClicked, CPUCharacters, _playerAbility);
    }

    private void EnemyAttack()
    {
        if (_gameState != GameState._cpuTurn) { return; }

        currentEnemy = _getRandomCharacter.RandomCharacter(CPUCharacters);
        currentPlayer = _getRandomCharacter.RandomCharacter(playerCharacters);

        _updateUi.TextLogger("CPU player " + currentEnemy.CharacterName + " selected. Attacking " + currentPlayer.CharacterName);

        IAbility _cpuAbility = currentEnemy.GetComponent<IAbility>();
        StartAttack(currentPlayer, playerCharacters, _cpuAbility);

    }

    private void StartAttack(Character enemy, List<Character> enemyDeck, IAbility ability)
    {
        ability.GroupAbility();
        ability.Ability(_gameManager, enemy);

        if (enemy.Dead)
            _updateUi.RemoveCharacter(enemy, enemyDeck);

        UpdateRound();
    }

    private void ClearCharacterBuffs()
    {
        for (int i = 0; i < CPUCharacters.Count; i++)
        {
            if (CPUCharacters[i].Buffed)
            {
                Character buffedCharacter = CPUCharacters[i];

                if (currentRound >= buffedCharacter.RoundBuffed + 2)
                {
                    buffedCharacter.RemoveDamageBonus();
                }

            }
        }

        for (int i = 0; i < playerCharacters.Count; i++)
        {
            if (playerCharacters[i].Buffed)
            {
                Character buffedCharacter = playerCharacters[i];

                if (currentRound >= buffedCharacter.RoundBuffed + 2)
                {
                    Debug.Log("Debuffing player " + buffedCharacter.CharacterName);
                    buffedCharacter.RemoveDamageBonus();
                }

            }
        }
    }

    public IEnumerator ComputerThinking(float seconds)
    {
        // Not AI simple delay for demo purpose. MiniMax AI logic might work well here.
        _updateUi.TextLogger("Computer is thinking.... " + currentRound);
        _updateUi.UpdateCanvasText();
        yield return new WaitForSeconds(seconds);

        EnemyAttack();
    }

    private void PlayersTurn()
    {
        currentPlayer = _getRandomCharacter.RandomCharacter(playerCharacters);
        _updateUi.TextLogger("Human Player " + currentPlayer.CharacterName + " selected to attack! ");

        CheckCharacterType(currentPlayer.GetComponent<IClassDetails>());
    }

    private void CheckCharacterType(IClassDetails classDetails)
    {
        if (classDetails.ClassDetails()["Class"] != "Support") { return; }
        
        _updateUi.TextLogger(currentPlayer.CharacterName + " is attempting to buff a team member");

        Character ce = _getRandomCharacter.RandomCharacter(CPUCharacters);
        IAbility _supportAblity = currentPlayer.GetComponent<IAbility>();

        StartAttack(ce, CPUCharacters, _supportAblity);
    }

    private void UpdateRound()
    {
        currentRound++;
        _updateUi.UpdateRound(currentRound.ToString());
        
        RunGame();

    }

    public void Update()
    {
        _updateUi.UpdateCanvasText();
    }

    public void RunGame()
    {
        if (CPUCharacters.Count > 0 && playerCharacters.Count > 0)
        {
            
            if (currentRound % 2 == 0)
            {
                _gameState = GameState._cpuTurn;
                Debug.Log(_gameState + " " + currentRound);
                StartCoroutine(ComputerThinking(3f));
            }
               
            else
            {
                _gameState = GameState._playersTurn;
                Debug.Log(_gameState + " " + currentRound);
                PlayersTurn();
            }
        }

        else
            GetWinner();
    }

    private void GetWinner()
    {
        Debug.Log("Looking for winner");

        if (IsListEmpty.IsEmpty(CPUCharacters))
            _updateUi.UpdateRound("Player Wins");

        if (IsListEmpty.IsEmpty(playerCharacters))
            _updateUi.UpdateRound("CPU Wins");

        _gameState = GameState._gameOver;
    }

    #endregion
}
