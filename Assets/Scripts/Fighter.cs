﻿using System.Collections.Generic;
using UnityEngine;

public enum FighterGroups { Pirate, Ninja }
public class Fighter : Character, IAbility, IClassDetails
{
    public string _characterText;
    public FighterGroups fighterGroups;
    public string CharacterClass = "Fighter";
    private Dictionary<string, string> classDetails;

    public void UpdateDetails()
    {
        switch (fighterGroups)
        {
            case FighterGroups.Ninja:
                _characterText = "Ninjas are not targetable every other round!";
                break;
            case FighterGroups.Pirate:
                _characterText = "Pirates attack twice on first round!";
                break;
        }
    }

    public Dictionary<string, string> ClassDetails()
    {
        UpdateDetails();
        classDetails = new Dictionary<string, string>();

        classDetails.Add("Class", CharacterClass);
        classDetails.Add("Group", fighterGroups.ToString());
        classDetails.Add("Ability", _characterText);

        return classDetails;
    }

    public void Ability(GameManager manager, Character enemy)
    {
        int round = manager.currentRound;

        switch (fighterGroups)
        {
            case FighterGroups.Ninja:
                if (round % 2 == 0)
                {
                    UpdateUI.TextLogger(enemy.CharacterName + " ABILITY: Every second round. NOT Targetable!");
                    IsTargetable(enemy);
                    Attack(enemy);
                    IsTargetable(enemy);
                }

                else
                {
                    UpdateUI.TextLogger("- NORMAL ATTACK NINJA");
                    Attack(enemy);
                }
                break;

            case FighterGroups.Pirate:
                if (round == 1)
                {
                    UpdateUI.TextLogger("PIRATE - Hits twice on the first round");
                    Attack(enemy);
                    Attack(enemy);
                }

                else
                {
                    UpdateUI.TextLogger("- NORMAL ATTACK PIRATE");
                    Attack(enemy);
                }
                break;
        }
    }

    public void GroupAbility()
    {
        //UpdateUI.TextLogger("GROUP ABILITY - NINJA AND PIRATE have no group ability");
    }

}